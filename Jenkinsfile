pipeline {
  agent {
    label "any"
  }
  environment {
    DOCKER     = credentials("${DOCKER_REPO_CREDS}")
    GCP_SA_KEY = credentials("${GCP_SA}")
  }
  stages {
    stage("Get Temporary Vault Access Token") {
      steps {
        script {
          def configuration = [$class: "VaultConfiguration",
                                vaultUrl: "https://vault.gcptechlab.ca:8200",
                                vaultCredentialId: "vault-creds"]
          def secrets = [
            [$class: "VaultSecret",
              path: "auth/token/lookup-self",
              secretValues: [
                [$class: "VaultSecretValue",
                  envVar: "VAULT_TOKEN",
                  vaultKey: "id"]]]
          ]
          wrap([$class: "VaultBuildWrapper", configuration: configuration, vaultSecrets: secrets]) {
            env.VAULT_TOKEN = "${VAULT_TOKEN}"
          }
        }
      }
    }
    stage("Build Docker Image") {
      when {
        allOf {
          equals expected: "deploy", actual: params.TERRAFORM_ACTION;
          equals expected: "dev", actual: params.ENVIRONMENT
        }
      }
      steps {
        script {
          sh "make build"
        }
      }
    }
    stage("Perform Tests") {
      when {
        equals expected: "deploy", actual: params.TERRAFORM_ACTION
      }
      steps {
        script {
          sh "make test"
        }
      }
    }
    stage("Publish Docker Image") {
      when {
        allOf {
          equals expected: "deploy", actual: params.TERRAFORM_ACTION;
          equals expected: "dev", actual: params.ENVIRONMENT
        }
      }
      steps {
        script {
          sh "make publish"
        }
      }
    }
    stage("Deploy") {
      when {
        equals expected: "deploy", actual: params.TERRAFORM_ACTION
      }
      steps {
        script {
          sh "make deploy"
        }
      }
    }
    stage("Destroy") {
      when {
        equals expected: "destroy", actual: params.TERRAFORM_ACTION
      }
      steps {
        script {
          timeout(time: 5, UNIT: "MINUTES") {
            def userInput = input(
              id: "userInput",
              message: "This action will destroy the environment! Continue?",
              parameters: [
                [$class: "BooleanParameterDefinition",
                  defaultValue: false,
                  description: "", name: "Please confirm you sure to proceed"]])
            if(!userInput) {
                error "Destruction was not confirmed"
            }
          }
          sh "make destroy"
        }
      }
    }
  }
  post {
    always {
      sh "make cleanup"
      deleteDir()
    }
  }
}
