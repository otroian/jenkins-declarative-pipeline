# Project variables
PROJECT_NAME ?= myapp
DOCKER_REPO_NAME ?= gcr.io

# Utility functions
INFO := @ bash -c 'echo "==> $$0 <=="'

.PHONY: build test publish deploy destroy cleanup

build:
	${INFO} "Building the image..."
	@ docker build -t $(PROJECT_NAME) -f ./docker/dockerfile .
	${INFO} "Build complete"

test:
	${INFO} "Testing the image for known vulnerabilities..."
	@ docker run --rm $(PROJECT_NAME) run_tests
	${INFO} "Testing complete"

publish:
	${INFO} "Pushing the image to the registry..."
	@ docker login $(DOCKER_REPO_NAME) -u ${DOCKER_USR} -p ${DOCKER_PSW}
	@ docker tag $(PROJECT_NAME) $(DOCKER_REPO_NAME)/$(PROJECT_NAME):$$(git rev-parse --short HEAD)
	@ docker push $(DOCKER_REPO_NAME)/$(PROJECT_NAME):$$(git rev-parse --short HEAD)
	@ docker logout
	${INFO} "Done"

deploy:
	${INFO} "Deploying..."
	@ terraform init -no-color -backend-config="terraform/backend.${ENVIRONMENT}.tfvars" -backend-config "access_key=${ACCESS_KEY}" terraform/
	@ terraform workspace select ${ENVIRONMENT} terraform/ || terraform workspace new ${ENVIRONMENT} terraform/
	@ terraform plan -out=main.tfplan -input=false -no-color -var-file="terraform/${ENVIRONMENT}.tfvars" -var "vault_token=${VAULT_TOKEN}" -var "app_image=$(DOCKER_REPO_NAME)/$(PROJECT_NAME):$$(git rev-parse --short HEAD)" -var "docker_registry=${DOCKER_REPO_NAME}" -var "docker_user=${DOCKER_USR}" -var "docker_password=${DOCKER_PSW}" terraform/
	@ terraform apply -no-color main.tfplan && rm -f main.tfplan
	${INFO} "Deployment complete"

destroy:
	${INFO} "Destroying..."
	@ terraform init -no-color -backend-config="terraform/backend.${ENVIRONMENT}.tfvars" -backend-config "access_key=${ACCESS_KEY}" terraform/
	@ terraform workspace select ${ENVIRONMENT} terraform/ || terraform workspace new ${ENVIRONMENT} terraform/
	@ terraform plan -destroy -out=main.tfplan -input=false -no-color -var-file="terraform/${ENVIRONMENT}.tfvars" -var "vault_token=${VAULT_TOKEN}" -var "app_image=$(DOCKER_REPO_NAME)/$(PROJECT_NAME):$$(git rev-parse --short HEAD)" -var "docker_registry=${DOCKER_REPO_NAME}" -var "docker_user=${DOCKER_USR}" -var "docker_password=${DOCKER_PSW}" terraform/
	@ terraform apply -no-color main.tfplan && rm -f main.tfplan
	${INFO} "Destroy complete"

cleanup:
	${INFO} "Destroying Running Containers..."
	@ docker stop $(PROJECT_NAME) 2>/dev/null
	${INFO} "Done"
